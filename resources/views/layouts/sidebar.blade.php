@if(!Auth::guest())
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/iau-logo.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p style="text-transform: uppercase;">@if(!empty($user_data)) {{$user_data->name}} @endif</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ (Request::is('dashboard') ? 'active' : '') }}">
                <a href="{{ url('/dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview {{ (Request::is('examiner/first-list') || Request::is('examiner/second-list') ? 'active' : '') }}">
                <a href="javascript:void(0);"><i class="fa fa-th"></i> <span>Examiner</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::is('examiner/first-list') ? 'active' : '') }}"><a href="{{ url('/examiner/first-list') }}"><i class="fa fa-circle-o"></i>1st Examiner List</a></li>
                    <li class="{{ (Request::is('examiner/second-list') ? 'active' : '') }}"><a href="{{ url('/examiner/second-list') }}"><i class="fa fa-circle-o"></i>2nd Examiner List</a></li>
                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
    @endif