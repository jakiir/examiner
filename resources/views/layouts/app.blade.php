<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Examiner') }} | @yield('title')</title>
    <meta name="keywords" content="IAU, iau, Islamic Arabic University, islamic arabic university, islamic, arabic, university, ইসলামি আরবি বিশ্ববিদ্যালয়, ইসলামি, আরবি, বিশ্ববিদ্যালয়, bangladesh islamic arabic university, banglades, bangla, nu, dhaka university, national university, ministry of education, islamic education, bangladesh islamic education, arabic education, islamic studies, du, madrasah education, madrasha education, quran, hadith, fiqah">
    <meta name="description" content="Islamic Arabic University, Graduation and Post Graduation From Madrasah.">
    <meta property="og:title" content="Islamic Arabic University, Dhaka, Bangladesh">
    <meta property="og:site_name" content="Islamic Arabic University">
    <meta property="og:description" content="Islamic Arabic University, Graduation and Post Graduation From Madrasah.">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://e.iau.edu.bd/">
    <meta property="og:image" content="{{asset('img/iau-logo-area.jpg')}}">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="315">
    <meta name="fb:app_id" property="fb:app_id" content="1875338662745164">
    <link rel="canonical" href="http://e.iau.edu.bd/">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <link rel="icon" type="image/ico" href="{{asset('img/favicon.ico')}}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.css')}}">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand-" href="{{ url('/') }}">
                        <img src="{{asset('img/iau-logo.png')}}" title="IAU Examiner" alt="IAU Examiner" width="100" style="margin-top:5px;"/>
                        {{--{{ config('app.name', 'Laravel') }}--}}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                            <li><a href="{{ URL::to('examiner/registration-form') }}">Examiner Form</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li><a href="{{ URL::to('examiner/registration-form') }}">Examiner Form</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js')}}"></script>
    @yield('footer-script')
</body>
</html>
