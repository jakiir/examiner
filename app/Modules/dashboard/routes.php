<?php

Route::group(array('module' => 'Dashboard', 'middleware' => ['web'], 'namespace' => 'App\Modules\Dashboard\Controllers'), function() {

    Route::get('/dashboard', "DashboardController@index");

});

