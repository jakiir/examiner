@extends('layouts.app')
@section('title', 'Examiner Form')
@section('content')
<div class="container">
    <div class="row">
        <div id="signupbox" class="mainbox col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">১ম পরীক্ষক/২য় পরীক্ষক নিয়োগের আবেদন</div>
                </div>
                <div class="panel-body" >
                    @if($errors->all())
                        <div class="alert alert-danger"><ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul></div>
                    @endif
                    {!! Form::open(['url' => 'examiner/store-examiner-data','method' => 'post','id' => 'eximinerAppData','role'=>'form','enctype'=>'multipart/form-data']) !!}

                    <div class="form-group col-md-12 {{$errors->has('type_of_examiner') ? 'has-error' : ''}}">
                        {!! Form::label('type_of_examiner','১। আবেদনের ধরণ : ',['class'=>'col-md-2 font-ok required-star']) !!}
                        <div class="controls col-md-4"  style="margin-bottom: 10px">
                            <label class="radio-inline">
                                {!! Form::radio('type_of_examiner', '1st', false, ['id' => 'type_of_examiner_1','required'=>"required"]) !!} ১ম পরীক্ষক
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('type_of_examiner', '2nd',false, ['id' => 'type_of_examiner_2','required'=>"required"]) !!} ২য় পরীক্ষক
                            </label>
                            {!! $errors->first('type_of_examiner','<span class="help-block">:message</span>') !!}
                        </div>

                        {!! Form::label('exam_division','বিভাগ : ',['class'=>'col-md-2 font-ok required-star']) !!}
                        <div class="controls col-md-4 "  style="margin-bottom: 10px">
                            <label class="radio-inline">
                                {!! Form::radio('exam_division', 'Hadith', false, ['id' => 'exam_division_1','required'=>"required"]) !!} হাদীস
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('exam_division', 'Tafsir', false, ['id' => 'exam_division_2','required'=>"required"]) !!} তাফসীর
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('exam_division', 'fiqh', false, ['id' => 'exam_division_3','required'=>"required"]) !!} ফিকহ
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('exam_division', 'Adab', false, ['id' => 'exam_division_4','required'=>"required"]) !!} আদব
                            </label>
                            {!! $errors->first('exam_division','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group col-md-12 {{$errors->has('subject_code') ? 'has-error' : ''}}">
                        <div class="controls col-md-12 group-headline">২। যে বিষয়ে ১ম পরীক্ষক/২য় পরীক্ষক হতে ইচ্ছুক : </div>
                        {!! Form::label('subject_code','বিষয়ের কোড : ',['class'=>'col-md-2 font-ok required-star']) !!}
                        <div class="controls col-md-3">
                            {!! Form::text('subject_code', null,['class'=>'form-control number required input-sm',
                                            'placeholder'=>'বিষয়ের কোড', 'maxlength'=>'3']) !!}
                            {!! $errors->first('subject_code','<span class="help-block">:message</span>') !!}
                        </div>
                        {!! Form::label('subject_name','বিষয়ের নাম : ',['class'=>'col-md-2 font-ok required-star']) !!}
                        <div class="controls col-md-5">
                            {!! Form::text('subject_name', null,['class'=>'form-control bnEng required input-sm',
                                            'placeholder'=>'বিষয়ের নাম', 'maxlength'=>'64']) !!}
                            {!! $errors->first('subject_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group col-md-12 {{$errors->has('index_number') ? 'has-error' : ''}}">
                        {!! Form::label('index_number','৩। এম.পি.ও ইনডেক্স/আইডি নম্বর : ',['class'=>'col-md-4 font-ok required-star']) !!}
                        <div class="controls col-md-6">
                            {!! Form::text('index_number', null,['class'=>'form-control number required input-sm',
                                            'placeholder'=>'এম.পি.ও ইনডেক্স/আইডি নম্বর', 'maxlength'=>'9']) !!}
                            {!! $errors->first('index_number','<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="controls col-md-2"  style="margin-bottom: 10px">
                            <label class="radio-inline">
                                {!! Form::radio('gender', 'male', false, ['id' => 'gender_male','required'=>"required"]) !!} পুরুষ
                            </label>
                            <label class="radio-inline">
                                {!! Form::radio('gender', 'female', false, ['id' => 'gender_female','required'=>"required"]) !!} মহিলা
                            </label>
                        </div>
                    </div>

                    <div class="form-group col-md-12 {{$errors->has('examiner_name_bn') ? 'has-error' : ''}}">
                        {!! Form::label('examiner_name_bn','৪। আবেদনকারীর নাম ( বাংলায় ) : ',['class'=>'col-md-4 font-ok required-star']) !!}
                        <div class="controls col-md-8">
                            {!! Form::text('examiner_name_bn', null,['class'=>'form-control bnEng required input-sm',
                                    'placeholder'=>'আবেদনকারীর নাম ( বাংলায় )', 'maxlength'=>'64']) !!}
                            {!! $errors->first('examiner_name_bn','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="form-group col-md-12 {{$errors->has('examiner_name_en') ? 'has-error' : ''}}">
                        {!! Form::label('examiner_name_en','৫। আবেদনকারীর নাম ( ইংরেজীতে ) : ',['class'=>'col-md-4 font-ok required-star']) !!}
                        <div class="controls col-md-8">
                            {!! Form::text('examiner_name_en', null,['class'=>'form-control bnEng required input-sm',
                                    'placeholder'=>'আবেদনকারীর নাম ( ইংরেজীতে )', 'maxlength'=>'64']) !!}
                            {!! $errors->first('examiner_name_en','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="controls col-md-12 group-headline">৬। চাকরী সংক্রান্ত বিবরণ : </div>
                            <div class="form-group col-md-12 {{$errors->has('designation') ? 'has-error' : ''}}">
                                {!! Form::label('designation','পদবী : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-3">
                                    {!! Form::text('designation', null,['class'=>'form-control bnEng required input-sm',
                                            'placeholder'=>'পদবী', 'maxlength'=>'30']) !!}
                                    {!! $errors->first('designation','<span class="help-block">:message</span>') !!}
                                </div>

                                {!! Form::label('date_of_join','শিক্ষকতায় যোগদানের তারিখ ( বেসরকারি মাদ্রাসার ক্ষেত্রে এম.পি.ও অনুযায়ী ) : ',['class'=>'col-md-2 font-ok required-star small-font']) !!}
                                <div class="controls col-md-5">
                                    {!! Form::text('date_of_join', null,['class'=>'form-control required input-sm datepicker',
                                            'placeholder'=>'শিক্ষকতায় যোগদানের তারিখ', 'maxlength'=>'30']) !!}
                                    {!! $errors->first('date_of_join','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group col-md-12 {{$errors->has('inistitute_name') ? 'has-error' : ''}}">
                                {!! Form::label('inistitute_name','প্রতিষ্ঠানের নাম : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-5">
                                    {!! Form::select('inistitute_name', $all_institutes, null, ['placeholder'=>'নির্বাচন করুন','class' => 'form-control input-sm required selectpicker', 'data-live-search'=>true]) !!}
                                    {!! $errors->first('inistitute_name','<span class="help-block">:message</span>') !!}
                                </div>
                                {!! Form::label('madrasah_eiin','মাদ্রাসার EIIN : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-3">
                                    {!! Form::select('madrasah_eiin', $all_institute_eiin, null, ['placeholder'=>'নির্বাচন করুন','class' => 'form-control input-sm required selectpicker', 'data-live-search'=>true]) !!}
                                    {!! $errors->first('madrasah_eiin','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group col-md-12 {{$errors->has('police_station') ? 'has-error' : ''}}">
                                {!! Form::label('examiner_district','জেলা : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-5">
                                    {!! Form::select('examiner_district', $all_district, null, ['class' => 'form-control input-sm required selectpicker','placeholder'=>'নির্বাচন করুন', 'data-live-search'=>true]) !!}
                                    {!! $errors->first('examiner_district','<span class="help-block">:message</span>') !!}
                                </div>

                                {!! Form::label('police_station','উপজেলা/থানা : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-3">
                                    {!! Form::select('police_station', [old('police_station')=>old('police_station')], old('police_station'), ['placeholder'=>'নির্বাচন করুন', 'class' => 'form-control input-sm required selectpicker' ,'data-live-search'=>true]) !!}
                                    {!! $errors->first('police_station','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-12 {{$errors->has('post_office_name') ? 'has-error' : ''}}">
                                {!! Form::label('post_office_name','ডাকঘর : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-5">
                                    {!! Form::text('post_office_name', null,['class'=>'form-control bnEng required input-sm',
                                            'placeholder'=>'ডাকঘর', 'maxlength'=>'64']) !!}
                                    {!! $errors->first('post_office_name','<span class="help-block">:message</span>') !!}
                                </div>

                                {!! Form::label('post_code','পোস্ট কোড : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-3">
                                    {!! Form::text('post_code', null,['class'=>'form-control number required input-sm',
                                            'placeholder'=>'পোস্ট কোড', 'maxlength'=>'64']) !!}
                                    {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group col-md-12 {{$errors->has('mobile_number') ? 'has-error' : ''}}">
                                {!! Form::label('mobile_number','মোবাইল নম্বর : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-3">
                                    {!! Form::text('mobile_number', null,['class'=>'form-control bnEng required input-sm',
                                            'placeholder'=>'মোবাইল নম্বর', 'maxlength'=>'64']) !!}
                                    {!! $errors->first('mobile_number','<span class="help-block">:message</span>') !!}
                                </div>
                                {!! Form::label('email_address','ই-মেইল : ',['class'=>'col-md-2 font-ok required-star']) !!}
                                <div class="controls col-md-5">
                                    {!! Form::email('email_address', null,['class'=>'form-control bnEng required input-sm',
                                            'placeholder'=>'ই-মেইল', 'maxlength'=>'64']) !!}
                                    {!! $errors->first('email_address','<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="controls col-md-12 group-headline">৭। পরীক্ষকের ছবি : </div>
                        <div class="row">
                            <div class="form-group col-md-12 {{$errors->has('designation') ? 'has-error' : ''}}">
                                {!! Form::label('designation','দয়া করে আপলোড : ',['class'=>'col-md-3 font-ok required-star']) !!}
                                <div class="col-md-6 col-sm-9">
                                    <div class="well well-sm">
                                        <div class="row">
                                            <div class="col-sm-6 col-md-4">
                                                <img src="/img/noimage.png" class="profile-user-img img-responsive img-circle"
                                                     alt="Profile Picture" id="user_pic" width="200"/>

                                            </div>
                                            <div class="col-sm-6 col-md-8">
                                                <h4>পরীক্ষকের ছবি</h4>
                                                <cite title="[File Format: *.jpg / *.png, File size(3-25)KB]">
                                                    <label class="col-lg-8 text-left">
                                                            <span style="font-size: 9px; color: grey">[File Format: *.jpg / *.png, File size(3-25)KB]</span>
                                                    </label>
                                                </cite>
                                                <small>
                                                    <cite title="[File Format: *.jpg / *.png, File size(3-25)KB]">
                                                        <label class="col-lg-8 text-left">
                                                            <span id="user_err" class="text-danger" style="font-size: 10px;"></span>
                                                        </label>
                                                    </cite>
                                                </small>
                                                <div class="clearfix"><br/></div>
                                                <label class="btn btn-primary btn-file">
                                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    Browse <input type="file" onchange="readURLUser(this);" name="examiner_photo" class="required" data-type="user"
                                                                  data-ref="" style="display: none;">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls col-md-8 ">
                            <button type="submit" class="btn btn-primary btn-md">
                                <span class="glyphicon glyphicon-send"></span> Submit
                            </button>
                        </div>
                        <div class="aab controls col-md-4">
                            <a href="/" class="btn btn-default btn-md">
                                <span class="glyphicon glyphicon-off"></span> Cancel
                            </a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-script')
    <script type="text/javascript">
        function readURLUser(input) {
            if (input.files && input.files[0]) {
                $("#user_err").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                    $("#user_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_pic').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        jQuery(document).ready(function($){
            $("#eximinerAppData").validate();

            var today = new Date();
            var yyyy = today.getFullYear();
            var mm = today.getMonth();
            var dd = today.getDate();
            $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: today,
                minDate: '01/01/' + (yyyy - 6)
            });


            $("#examiner_district").change(function () {
                $(this).after('<span class="loading_data">Loading...</span>');
                var districtId = $(this).val();
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "/examiner/get-police-stations",
                    data: {
                        districtId: districtId
                    },
                    success: function (response) {
                        var option = '<option value="">নির্বাচন করুন</option>';
                        $("#police_station").empty();
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + value + '">' + value + '</option>';
                            });
                        }
                        $(self).parent().parent().parent().find("#police_station").append(option);
                        $('#police_station').selectpicker('refresh');
                        $(self).next().hide();
                    }
                });
            });
            //$("#examiner_district").trigger('change');

            $("#inistitute_name").change(function () {
                $(this).after('<span class="loading_data">Loading...</span>');
                var madrasahId = $(this).val();
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "/examiner/get-madrasah-eiin",
                    data: {
                        madrasahId: madrasahId
                    },
                    success: function (response) {
                        var option = '';
                        //$("#madrasah_eiin").empty();
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                                //option += '<option value="' + value + '">' + value + '</option>';
                                $("#madrasah_eiin option[value='"+value+"']").prop('selected', true);
                            });
                        }
                        //$(self).parent().parent().parent().find("#madrasah_eiin").append(option);
                        $('#madrasah_eiin').selectpicker('refresh');
                        $(self).next().remove();
                    }
                });
            });
            $("#madrasah_eiin").change(function () {
                $(this).after('<span class="loading_data">Loading...</span>');
                var madrasahId = $(this).val();
                var self = $(this);
                $.ajax({
                    type: "GET",
                    url: "/examiner/get-madrasah-name",
                    data: {
                        madrasahId: madrasahId
                    },
                    success: function (response) {
                        var option = '';
                        $("#inistitute_name option[value='a']").text("Val 1.0");
                        //$("#inistitute_name").empty();
                        if (response.responseCode == 1) {
                            $.each(response.data, function (id, value) {
                               // option += '<option value="' + value + '">' + value + '</option>';
                                $("#inistitute_name option[value='"+value+"']").prop('selected', true);
                            });
                        }
                        //$(self).parent().parent().parent().find("#inistitute_name").append(option);
                        $('#inistitute_name').selectpicker('refresh');
                        $(self).next().remove();
                    }
                });
            });
            //$("#inistitute_name").trigger('change');
        });
    </script>
@endsection