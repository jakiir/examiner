@extends('layouts.dashboard')
@section('title', '2nd Examiner List')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @yield('title')
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">@yield('title')</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Second Examiner List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="secondExaminerList" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>নাম</th>
                                <th>বিভাগ</th>
                                <th>বিষয়ের কোড</th>
                                <th>বিষয়ের নাম</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($get_second_examiner))
                                @foreach($get_second_examiner as $secondExaminer)
                                    <tr>
                                        <td>{{$secondExaminer->examiner_name_en}}</td>
                                        <td>{{$secondExaminer->exam_division}}</td>
                                        <td>{{$secondExaminer->subject_code}}</td>
                                        <td>{{$secondExaminer->subject_name}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>নাম</th>
                                <th>বিভাগ</th>
                                <th>বিষয়ের কোড</th>
                                <th>বিষয়ের নাম</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('footer-script')
    <script>
        $(function () {
            $("#secondExaminerList").DataTable();
            $('#datatable2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endsection