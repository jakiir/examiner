<?php

namespace App\Modules\Examiner\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Examiner\Models\Examiner;
use App\User;
use Illuminate\Http\Request;
use App\AreaInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class ExaminerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_institutes = DB::table('institutes')->pluck('institute_name','institute_name');
        $all_institute_eiin = DB::table('institutes')->pluck('eiin','eiin');
        $all_district = AreaInfo::where('area_type','=','2')->pluck('area_nm','area_nm');
        return view('examiner::examiner-regi-form', compact('all_institutes','all_institute_eiin','all_district'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExaminerData(Request $request, Examiner $examiner)
    {
        $this->validate($request, [
            'type_of_examiner' => 'required',
            'exam_division' => 'required',
            'subject_code' => 'required|numeric',
            'subject_name' => 'required|max:64',
            'index_number' => 'required|max:10',
            'gender' => 'required',
            'examiner_name_bn' => 'required',
            'examiner_name_en' => 'required',
            'designation' => 'required',
            'date_of_join' => 'required',
            'inistitute_name' => 'required',
            'madrasah_eiin' => 'required|numeric',
            'examiner_district' => 'required',
            'police_station' => 'required',
            'post_office_name' => 'required',
            'post_code' => 'required',
            'mobile_number' => 'required|max:15',
            'email_address' => 'required',
        ]);
        $madrasah = DB::table('institutes')
            ->where('institute_name', $request->get('inistitute_name'))
            ->where('eiin', $request->get('madrasah_eiin'))->first();
        if(empty($madrasah))
            return redirect('examiner/registration-form')->withInput();

        try {
            DB::beginTransaction();
            $imgUrl = '';
            $data = $request->all();
            if(!empty($data['examiner_photo'])){
                $image = $data['examiner_photo'];
                $indextId = $request->get('index_number');
                $madrasah_eiin = $request->get('madrasah_eiin');
                $extension = $image->getClientOriginalExtension();
                $filename  = $indextId.'.'.$extension;
                $savedPath = "pic/{$madrasah_eiin}/";
                $path = public_path($savedPath);
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $img = Image::make($image->getRealPath());
                $img->resize(null, 160, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $savedPath2 = "pic/{$madrasah_eiin}/{$filename}";
                $path2 = public_path($savedPath2);
                $img->save($path2);
                $imgUrl = '/'.$savedPath2;
            }

            $examiner->type_of_examiner = $request->get('type_of_examiner');
            $examiner->exam_division = $request->get('exam_division');
            $examiner->subject_code = $request->get('subject_code');
            $examiner->subject_name = $request->get('subject_name');
            $examiner->index_number = $request->get('index_number');
            $examiner->gender = $request->get('gender');
            $examiner->examiner_name_bn = $request->get('examiner_name_bn');
            $examiner->examiner_name_en = $request->get('examiner_name_en');
            $examiner->designation = $request->get('designation');
            $examiner->date_of_join = $request->get('date_of_join');
            $examiner->inistitute_name = $request->get('inistitute_name');
            $examiner->madrasah_eiin = $request->get('madrasah_eiin');
            $examiner->examiner_district = $request->get('examiner_district');
            $examiner->police_station = $request->get('police_station');
            $examiner->post_office_name = $request->get('post_office_name');
            $examiner->post_code = $request->get('post_code');
            $examiner->mobile_number = $request->get('mobile_number');
            $examiner->email_address = $request->get('email_address');
            $examiner->examiner_photo = $imgUrl;
            $examiner->save();



            DB::commit();
            return redirect('examiner/examiner-thank-you/'.$request->get('madrasah_eiin'));
        }
        catch (\Exception $e)
        {
            DB::rollback();
            echo $e->getMessage() . '-' . $e->getLine();
            Session::flash('error', 'Sorry! Somthing Wrong.');
            return redirect('/examiner/registration-form');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function firstExaminerList(){
        $userId = Auth::id();
        $user_data = User::where('id','=',$userId)->first(['id','user_type','name','email','designation','created_at','updated_at']);
        $get_first_examiner = Examiner::where('type_of_examiner','=','1st')->get();
        return view('examiner::examiner-list-1',compact('get_first_examiner','user_data'));
    }

    public function secondExaminerList(){
        $userId = Auth::id();
        $user_data = User::where('id','=',$userId)->first(['id','user_type','name','email','designation','created_at','updated_at']);
        $get_second_examiner = Examiner::where('type_of_examiner','=','2nd')->get();
        return view('examiner::examiner-list-2',compact('get_second_examiner','user_data'));
    }

    public function examinerThankYou($eiin){
        if(empty($eiin)) die('You have no access right! Please contact with system admin if you have any query.');
        return view('examiner::examiner-thank-you');
    }

    public function getMadrasahEiin(Request $request) {
        $madrasahField = 'institutes.institute_name';
        $data = ['responseCode' => 0, 'data' => ''];
        $madrasah = DB::table('institutes')->where($madrasahField, $request->get('madrasahId'))->first();
        if ($madrasah) {
            $madrasahId = $madrasah->id;
            $get_data = DB::table('institutes')->where('id', DB::raw($madrasahId))
                ->whereNotNull($madrasahField)
                ->select('institutes.eiin')
                ->orderBy($madrasahField)
                ->first(['institutes.eiin']);
            $data = ['responseCode' => 1, 'data' => $get_data];
        }
        return response()->json($data);
    }

    public function getMadrasahName(Request $request) {
        $madrasahField = 'institutes.eiin';
        $data = ['responseCode' => 0, 'data' => ''];
        $madrasah = DB::table('institutes')->where($madrasahField, $request->get('madrasahId'))->first();
        if ($madrasah) {
            $madrasahId = $madrasah->id;
            $get_data = DB::table('institutes')->where('id', DB::raw($madrasahId))
                ->whereNotNull($madrasahField)
                ->select('institutes.institute_name')
                ->orderBy($madrasahField)
                ->first(['institutes.institute_name']);
            $data = ['responseCode' => 1, 'data' => $get_data];
        }
        return response()->json($data);
    }

    public function getPoliceStations(Request $request) {
        $areaField = 'area_info.area_nm';
        $data = ['responseCode' => 0, 'data' => ''];
        $area = AreaInfo::where($areaField, $request->get('districtId'))->where('area_type', 2)->first();
        if ($area) {
            $area_id = $area->area_id;
            $get_data = AreaInfo::where('pare_id', DB::raw($area_id))
                ->whereNotNull($areaField)
                ->where('area_type', 3)
                ->select($areaField)
                ->orderBy($areaField)
                ->pluck($areaField);
            $data = ['responseCode' => 1, 'data' => $get_data];
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
