<?php

Route::group(array('module' => 'Examiner', 'middleware' => ['web'], 'namespace' => 'App\Modules\Examiner\Controllers'), function() {

    Route::get('/examiner/registration-form', 'ExaminerController@index')->name('Examiner Form');
	Route::get('/', 'ExaminerController@index')->name('Examiner Form');
    Route::post('/examiner/store-examiner-data', 'ExaminerController@storeExaminerData');
    Route::get('/examiner/examiner-thank-you/{eiin}', 'ExaminerController@examinerThankYou');
    Route::get('/examiner/get-madrasah-eiin', 'ExaminerController@getMadrasahEiin');
    Route::get('/examiner/get-madrasah-name', 'ExaminerController@getMadrasahName');
    Route::get('/examiner/get-police-stations', 'ExaminerController@getPoliceStations');
    Route::get('/examiner/first-list', 'ExaminerController@firstExaminerList');
    Route::get('/examiner/second-list', 'ExaminerController@secondExaminerList');
    Route::resource('examiner','examinerController');

});

