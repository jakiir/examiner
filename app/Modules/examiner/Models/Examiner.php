<?php

namespace App\Modules\Examiner\Models;

use Illuminate\Database\Eloquent\Model;

class Examiner extends Model
{
    protected $table = 'examiner_info';
    protected $fillable = array(
        'id',
        'type_of_examiner',
        'exam_division',
        'subject_code',
        'subject_name',
        'index_number',
        'gender',
        'examiner_name_bn',
        'examiner_name_en',
        'designation',
        'date_of_join',
        'inistitute_name',
        'madrasah_eiin',
        'examiner_district',
        'police_station',
        'post_office_name',
        'post_code',
        'mobile_number',
        'email_address',
        'examiner_photo',
        'created_at',
        'updated_at',
    );

//    public static function boot() {
//        parent::boot();
//        static::creating(function($post) {
//            $post->created_by = CommonFunction::getUserId();
//            $post->updated_by = CommonFunction::getUserId();
//        });
//
//        static::updating(function($post) {
//            $post->updated_by = CommonFunction::getUserId();
//        });
//    }
}
